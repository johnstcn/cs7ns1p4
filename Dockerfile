FROM python:3-slim

WORKDIR /
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

ENV VEHICLE_NAME "car1"
ENV SQS_URL "http://sqs:9324"
ENV AWS_ACCESS_KEY_ID "fakekey"
ENV AWS_SECRET_ACCESS_KEY "fakesecret"
ENV AWS_DEFAULT_REGION "dummy"

COPY ./vehicle_simulator /app
CMD ["sh", "-c", "python /app/server.py $VEHICLE_NAME --sqs $SQS_URL"]