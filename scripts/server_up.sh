#!/usr/bin/env bash

docker run --name car1 -d --net=host -e SQS_URL=http://localhost:9324 -e VEHICLE_NAME=car1 cs7ns1p4:latest
docker run --name car2 -d --net=host -e SQS_URL=http://localhost:9324 -e VEHICLE_NAME=car2 cs7ns1p4:latest
docker run --name car3 -d --net=host -e SQS_URL=http://localhost:9324 -e VEHICLE_NAME=car3 cs7ns1p4:latest
docker run --name car4 -d --net=host -e SQS_URL=http://localhost:9324 -e VEHICLE_NAME=car4 cs7ns1p4:latest
docker run --name car5 -d --net=host -e SQS_URL=http://localhost:9324 -e VEHICLE_NAME=car5 cs7ns1p4:latest