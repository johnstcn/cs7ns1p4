#!/usr/bin/env bash
set -euo pipefail
docker run --name sqs-local -d -p 9324:9324 vsouza/sqs-local:latest
aws --endpoint-url=http://localhost:9324 sqs create-queue --queue-name car1_state_input
aws --endpoint-url=http://localhost:9324 sqs create-queue --queue-name car2_state_input
aws --endpoint-url=http://localhost:9324 sqs create-queue --queue-name car3_state_input
aws --endpoint-url=http://localhost:9324 sqs create-queue --queue-name car4_state_input
aws --endpoint-url=http://localhost:9324 sqs create-queue --queue-name car5_state_input
aws --endpoint-url=http://localhost:9324 sqs create-queue --queue-name car1_state_output
aws --endpoint-url=http://localhost:9324 sqs create-queue --queue-name car2_state_output
aws --endpoint-url=http://localhost:9324 sqs create-queue --queue-name car3_state_output
aws --endpoint-url=http://localhost:9324 sqs create-queue --queue-name car4_state_output
aws --endpoint-url=http://localhost:9324 sqs create-queue --queue-name car5_state_output