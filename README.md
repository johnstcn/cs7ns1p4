# AWS Vehicle Simulator

This project simulates a number of vehicles. It consists of a client and multiple servers, which communicate over SQS.

## Setup

Either using `pipenv`:
```python
pipenv install
``` 

Or `pip`: 
```python
pip install -r requirements.dev.txt
```

Some SQS queues need to exist for each vehicle you want to simulate:
 - `${VEHICLE_NAME}_state_input`
 - `${VEHICLE_NAME}_state_output`

The `client.py` script will attempt to create these for you, if they do not exist.

**NOTE: these queues are purged upon startup to ensure consistency.**

## Scripts

### client.py

This script handles passing state to and from a number of vehicles.

Basic usage: 
```
client.py <initial state file> <list of vehicles to simulate> --ticks <num_ticks>
```

For example, the following will simulate two cars with initial state from `state.json` for two ticks:
```
client.py state.json car0 car1 --ticks 2
```

Additional arguments:
 - `--sqs`: use a different SQS endpoint URL. See below for more details on using this.
 - `--pause`: pause after each tick.

### server.py

Simulates a single vehicle, reading state from a given input queue and writing to a given output queue.

```
server.py <vehicle name>
```

Additional arguments:
 - `--sqs`: use a different SQS endpoint URL. See below for more details on using this.
 - `--pause`: pause after each tick.


## Bonus Round: Local SQS

If you have `docker` installed, you can run a local SQS instance:

```
docker run -d -p 9324:9324 vsouza/sqs-local:latest 
```

Then, you can create the input queues like so, for example given vehicle `car0`:

```
aws --endpoint-url=http://localhost:9324 sqs create-queue --queue-name car0_state_input
aws --endpoint-url=http://localhost:9324 sqs create-queue --queue-name car0_state_output
```

Verify the queues are present locally:

```
aws --endpoint-url=http://localhost:9324 sqs list-queues
{
    "QueueUrls": [
        "http://localhost:9324/queue/car0_state_output",
        "http://localhost:9324/queue/car0_state_input"
    ]
}
```

Then run the client/server with the same `--sqs=http://localhost:9324` argument to make it talk to your local SQS. 

## Bonus bonus round: Docker

Again, if you have `docker` installed, you can run the server as follows:


Firstly, build the container:
```
docker build . -t cs7ns1p4:latest
```

Then, run it. In this example, we are using the local SQS as described above:
```
docker run -it --net=host -e SQS_URL=http://localhost:9324 -e VEHICLE_NAME=car1 cs7ns1p4:latest
```

If you're feeling fancy, you can of course hook this up into `docker-compose`. 
