#!/usr/bin/env python3

def speed(curr_state):
    output = {}
    for key in curr_state:
        if not key.startswith("car"):
            continue
        veh_state = curr_state[key]
        if veh_state["speed"] > curr_state["threshold"]["speed"]:
            output[key] = "overspeeding"
    return output


def air_quality_level(curr_state):
    output = {}
    for key in curr_state:
        if not key.startswith("car"):
            continue
        veh_state = curr_state[key]
        if veh_state["air_quality_level"] < curr_state["threshold"]["air_quality_level"]:
            output[key] = "below_threshold"
    return output


def fuel_level(curr_state):
    output = {}
    for key in curr_state:
        if not key.startswith("car"):
            continue
        veh_state = curr_state[key]
        curr_fuel = veh_state["fuellevel"]
        fuel_thresh = curr_state["threshold"]["fuel_level"]
        if curr_fuel <= 0:
            output[key] = "fuel_empty"
        elif curr_fuel < fuel_thresh:
            output[key] = "fuel_low"
    return output


def lane_departure_system(curr_state):
    output = {}
    for key in curr_state:
        if not key.startswith("car"):
            continue
        veh_state = curr_state[key]
        if veh_state["lanewidth"]-veh_state["carwidth"] < curr_state["threshold"]["lane_departure_system"]:
            output[key] = "Crossing lane"
    return output


def light(curr_state):
    output = {}
    for key in curr_state:
        if not key.startswith("car"):
            continue
        veh_state = curr_state[key]
        if veh_state["brightness"] < curr_state["threshold"]["light"]:
            output[key] = "Brightness is low"
    return output


def car_weight(curr_state):
    output = {}
    for key in curr_state:
        if not key.startswith("car"):
            continue
        veh_state = curr_state[key]
        if veh_state["car_weight"] > curr_state["threshold"]["car_weight"]:
            output[key] = "overloaded"
    return output


def power_level(curr_state):
    output = {}
    for key in curr_state:
        if not key.startswith("car"):
            continue
        veh_state = curr_state[key]
        curr_power = veh_state["powerlevel"]
        power_thresh = curr_state["threshold"]["power_level"]
        if curr_power <= 0:
            output[key] = "power_empty"
        elif curr_power < power_thresh:
            output[key] = "power_low"
    return output


def prec_level(curr_state):
    output = {}
    for key in curr_state:
        if not key.startswith("car"):
            continue
        veh_state = curr_state[key]
        if veh_state["prec_level"] > curr_state["threshold"]["prec_level"]:
            output[key] = "high_precip"
    return output


def proximity(curr_state):
    output = {}
    cars=[i for i in curr_state.keys() if i.startswith('car')]
    for i in range(len(cars)):
        close_cars_this=[]
        for j in range(len(cars)):
            if(cars[i]!=cars[j] and abs(curr_state[cars[i]]['location']-(curr_state[cars[j]]['location']))<=curr_state["threshold"]["proximity"]):
                close_cars_this.append(cars[j])
        
        if len(close_cars_this)>0:
            output[cars[i]] = close_cars_this

    return output


def signal_level(curr_state):
    output = {}
    for key in curr_state:
        if not key.startswith("car"):
            continue
        veh_state = curr_state[key]
        if veh_state["signal_level"] < curr_state["threshold"]["signal_level"]:
            output[key] = "Signal strength is weak"
    return output
