#!/usr/bin/env python3

import actions
import log
from typing import *

LOG = log.init_logging("analysis")

def speed(vehicle: str, warnings: Dict) -> Set[str]:
    new_actions: Set[str] = set()
    if vehicle not in warnings.get("speed", {}):
        return new_actions

    speed_warning = warnings["speed"][vehicle]
    if speed_warning:
        LOG.warning("{0}: overspeeding".format(vehicle))
        new_actions.add(actions.DECELERATE)
    else:
        LOG.warning("{0} unhandled speed warning: {1}".format(vehicle, speed_warning))
    return new_actions


def power(vehicle: str, warnings: Dict) -> Set[str]:
    new_actions: Set[str] = set()
    if vehicle not in warnings.get("power_level", {}):
        return new_actions

    power_warning = warnings["power_level"][vehicle]
    if power_warning == "power_empty":
        LOG.warning("{0}: power empty".format(vehicle))
        new_actions.add(actions.DIE)
    elif power_warning == "power_low":
        LOG.warning("{0}: power low".format(vehicle))
        new_actions.add(actions.DECELERATE)
    else:
        LOG.warning("{0} unhandled power warning: {1}".format(vehicle, power_warning))
    return new_actions


def fuel_level(vehicle: str, warnings: Dict) -> Set[str]:
    new_actions: Set[str] = set()
    if vehicle not in warnings.get("fuel_level", {}):
        return new_actions

    fuel_warning = warnings["fuel_level"][vehicle]
    if fuel_warning == "fuel_empty":
        LOG.warning("{0}: fuel empty".format(vehicle))
        new_actions.add(actions.DIE)
    elif fuel_warning == "fuel_low":
        LOG.warning("{0}: fuel low".format(vehicle))
        new_actions.add(actions.DECELERATE)
    else:
        LOG.warning("{0} unhandled fuel warning: {1}".format(vehicle, fuel_warning))
    return new_actions


def load(vehicle: str, warnings: Dict) -> Set[str]:
    new_actions: Set[str] = set()
    if vehicle not in warnings.get("car_weight", {}):
        return new_actions

    load_warning = warnings["car_weight"][vehicle]
    if load_warning == "overloaded":
        new_actions.add(actions.DECELERATE)
    return new_actions


def precipitation(vehicle: str, warnings: Dict) -> Set[str]:
    new_actions: Set[str] = set()
    if vehicle not in warnings.get("prec_level", {}):
        return new_actions

    precip_warning = warnings["prec_level"][vehicle]
    if precip_warning:
        new_actions.add(actions.DECELERATE)
    return new_actions


def lane_departure_proximity(vehicle: str, curr_state: Dict) -> Set[str]:
    new_actions: Set[str] = set()
    departing_lane = vehicle in curr_state["warnings"]["lane_departure_system"]
    if not departing_lane:
        return new_actions

    curr_lane = curr_state[vehicle]["lane"]
    adjacent_lanes = [curr_lane - 1, curr_lane + 1]
    close_vehicle_keys = curr_state["warnings"]["proximity"].get(vehicle, [])
    close_vehicles = {
        curr_state[v]["lane"]: v
        for v in close_vehicle_keys
        if curr_state[v]["lane"] in adjacent_lanes
    }
    if close_vehicles:
        LOG.warning(
            "{0} close and adjacent vehicles: {1}".format(vehicle, close_vehicles)
        )
        new_actions.add(actions.DECELERATE)
    return new_actions
