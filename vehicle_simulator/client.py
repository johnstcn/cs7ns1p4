#!/usr/bin/env python3
import argparse
import json
import logging
import sys
import time

import boto3


def initial_state(state_path):
    with open(state_path, "r") as f:
        return json.load(f)


def init_logging(name):
    log = logging.getLogger(name)
    log.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stderr)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    handler.setFormatter(formatter)
    log.addHandler(handler)
    return log


class SimulationClient(object):
    def __init__(self, sqs_client, vehicles):
        self._log = init_logging("SimulationClient")
        self._sqs = sqs_client
        self._vehicles = vehicles.copy()
        self._input_queues = {}
        self._output_queues = {}

        for v in self._vehicles:
            self._input_queues[v] = self.__ensure_queue("{0}_state_input".format(v))
            self._output_queues[v] = self.__ensure_queue("{0}_state_output".format(v))

        ## XXX: this is ugly
        self.__purge_all_queues()


    def __ensure_queue(self, qname):
        qurl = None
        try:
            qurl = self._sqs.get_queue_url(QueueName=qname)['QueueUrl']
        except:
            q = self._sqs.create_queue(QueueName=qname)
            qurl = q['QueueUrl']
            self._log.info("created queue: {0}".format(qname))
        return qurl


    def __purge_all_queues(self):
        all_queues = sorted(
            list(self._input_queues.values()) + list(self._output_queues.values())
        )
        for queue in all_queues:
            self._sqs.purge_queue(QueueUrl=queue)
            self._log.info("purged queue {0}".format(queue))

    def __send_states(self, curr_state):
        enc_state = json.dumps(curr_state)
        for v, input_queue in sorted(self._input_queues.items()):
            send_resp = self._sqs.send_message(
                QueueUrl=input_queue, MessageBody=enc_state
            )
            self._log.info(
                "sent messageid {0} to {1}".format(send_resp["MessageId"], input_queue)
            )

    def __recv_states(self):
        new_states = {}
        for v, output_queue in sorted(self._output_queues.items()):
            msg = self._sqs.receive_message(
                QueueUrl=output_queue, WaitTimeSeconds=10, MaxNumberOfMessages=1
            ).get("Messages", [])
            if len(msg) == 0:
                self._log.warning(
                    "no response from vehicle {0} via {1}".format(v, output_queue)
                )
                continue
            receipt_handle = msg[0]["ReceiptHandle"]
            self._sqs.delete_message(
                QueueUrl=output_queue, ReceiptHandle=receipt_handle
            )
            self._log.info(
                "acked messageid {0} from {1}".format(receipt_handle, output_queue)
            )
            new_states[v] = json.loads(msg[0]["Body"])
        return new_states

    def __merge_states(self, curr_state, new_states):
        merged = curr_state.copy()
        for vehicle, new_state in new_states.items():
            merged["warnings"].update(new_state["warnings"])
            merged[vehicle].update(new_state[vehicle])
        return merged

    def tick(self, curr_state):
        self.__send_states(curr_state)
        updated_states = self.__recv_states()
        return self.__merge_states(curr_state, updated_states)


def print_state_summary(final_state, vehicle):
    print("=== FINAL STATE: {0} ===".format(vehicle))
    vehicle_state = final_state[vehicle]
    print(json.dumps(vehicle_state, indent=4))


def log_vehicle_states(vehicles, ntick, curr_state):
    for sensor_key, sensor_warning in curr_state["warnings"].items():
        if not sensor_warning:
            continue
        print("tick {0}: sensor {1} warnings: {2}".format(ntick, sensor_key, sensor_warning))

    for vehicle in vehicles:
        print(
            "tick {0}: vehicle: {1} speed: {2} fuellevel: {3} powerlevel: {4} car_weight: {5} prec_level: {6} location: {7} dead: {8}".format(
                ntick,
                vehicle,
                curr_state[vehicle]["speed"],
                curr_state[vehicle]["fuellevel"],
                curr_state[vehicle]["powerlevel"],
                curr_state[vehicle]["car_weight"],
                curr_state[vehicle]["prec_level"],
                curr_state[vehicle]["location"],
                curr_state[vehicle]["dead"],
            )
        )


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("state_path", type=str, help="path to initial state file")
    parser.add_argument(
        "vehicles", type=str, nargs="+", help="names of vehicles to simulate"
    )
    parser.add_argument("--ticks", type=int, help="number of ticks", default=1)
    parser.add_argument("--pause", type=int, help="pause for this many seconds after each tick", default=0)
    parser.add_argument(
        "--sqs",
        type=str,
        help="SQS endpoint URL if developing locally e.g. http://localhost:9324",
    )
    args = parser.parse_args()

    sqs_client = boto3.client("sqs", endpoint_url=args.sqs)
    states = [initial_state(args.state_path)]

    sc = SimulationClient(sqs_client, args.vehicles)
    for t in range(args.ticks):
        try:
            curr_state = states[-1].copy()
            new_state = sc.tick(curr_state)
            states.append(new_state)
            log_vehicle_states(args.vehicles, t + 1, new_state)
            if args.pause > 0:
                time.sleep(args.pause)
        except KeyboardInterrupt:
            break
    for vehicle in args.vehicles:
        print_state_summary(states[-1], vehicle)


if __name__ == "__main__":
    main()
