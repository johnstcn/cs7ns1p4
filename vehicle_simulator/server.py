#!/usr/bin/env python3

import argparse
import json
import logging
import sys

import boto3

import sensors
import analysis
import actions


def init_logging(name):
    log = logging.getLogger(name)
    log.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stderr)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    handler.setFormatter(formatter)
    log.addHandler(handler)
    return log


def hack(curr_state):
    # decrease fuel level by 1 for each car
    for key in curr_state.keys():
        if not key.startswith("car"):
            continue
        if curr_state[key]["dead"]:
            continue
        curr_state[key]["powerlevel"] -= 1
        curr_state[key]["fuellevel"] -= 1
        curr_state[key]["location"] += curr_state[key]["speed"] 
    return curr_state


class VehicleSimulator(object):
    def __init__(self, vehicle, sensor_funcs, sqs_client):
        self._log = init_logging("VehicleSimulator:" + vehicle)
        self._sqs = sqs_client
        self._vehicle = vehicle
        self._sensor_funcs = sensor_funcs
        self._input_queue_url = self._sqs.get_queue_url(
            QueueName="{0}_state_input".format(self._vehicle)
        )["QueueUrl"]
        self._output_queue_url = self._sqs.get_queue_url(
            QueueName="{0}_state_output".format(self._vehicle)
        )["QueueUrl"]
        self._log.info(
            "input:{0} output:{1}".format(self._input_queue_url, self._output_queue_url)
        )

    def tick(self):
        curr_state = self.__recv_state()
        self._log.debug("current state: " + json.dumps(curr_state))
        new_state = curr_state.copy()
        new_state.update(self.__sense(curr_state))
        curr_actions = self.__analyze(new_state)
        self._log.debug("actions: " + ",".join(curr_actions))
        new_state = self.__actuate(curr_actions, new_state)
        self._log.debug("new state: " + json.dumps(curr_state))
        self.__send_state(new_state)

    def __recv_state(self):
        msg = None
        while True:
            msg = self._sqs.receive_message(
                QueueUrl=self._input_queue_url,
                WaitTimeSeconds=10,
                MaxNumberOfMessages=1,
            ).get("Messages", [])
            if len(msg) != 0:
                break
            else:
                self._log.warning("no state received after waiting 10 seconds")

        receipt_handle = msg[0]["ReceiptHandle"]
        self._sqs.delete_message(
            QueueUrl=self._input_queue_url, ReceiptHandle=receipt_handle
        )
        self._log.info(
            "acked messageid {0} from {1}".format(receipt_handle, self._input_queue_url)
        )
        return json.loads(msg[0]["Body"])

    def __send_state(self, new_state):
        payload = json.dumps(new_state)
        self._sqs.send_message(
            QueueUrl=self._output_queue_url, MessageAttributes={}, MessageBody=payload
        )

    def __sense(self, curr_state):
        tmp = hack(curr_state.copy())
        for sname, sfunc in self._sensor_funcs.items():
            tmp["warnings"][sname] = sfunc(tmp)
        return tmp

    def __analyze(self, curr_state):
        warnings = curr_state["warnings"]
        actions = set([])
        actions.update(analysis.speed(self._vehicle, warnings))
        actions.update(analysis.power(self._vehicle, warnings))
        actions.update(analysis.fuel_level(self._vehicle, warnings))
        actions.update(analysis.load(self._vehicle, warnings))
        actions.update(analysis.precipitation(self._vehicle, warnings))
        actions.update(analysis.lane_departure_proximity(self._vehicle, curr_state))
        return actions


    def __actuate(self, curr_actions, curr_state):
        new_state = curr_state.copy()
        for action in curr_actions:
            if action == actions.DIE:
                curr_state[self._vehicle]["dead"] = True
                break
            elif action == actions.DECELERATE:
                curr_speed = curr_state[self._vehicle]["speed"]
                speed_threshold = curr_state["threshold"]["speed"]
                if curr_speed <= speed_threshold:
                    continue

                curr_weight = curr_state[self._vehicle]["car_weight"]
                weight_threshold = curr_state["threshold"]["car_weight"]
                load_factor = max(0, int(curr_weight - weight_threshold) * 0.2)
                self._log.info("{0} load factor: {1}".format(self._vehicle, load_factor))

                curr_precip = curr_state[self._vehicle]["prec_level"]
                precip_threshold = curr_state["threshold"]["prec_level"]
                precip_factor = max(0, int(curr_precip - precip_threshold) * 0.2)
                self._log.info("{0} precip factor: {1}".format(self._vehicle, precip_factor))

                delta_speed = curr_state["threshold"]["deceleration_rate"]
                delta_speed_modified = max(delta_speed - load_factor - precip_factor, 1)
                min_speed = curr_state["threshold"]["speed"]
                new_speed = max(min_speed, curr_speed - delta_speed_modified)
                curr_state[self._vehicle]["speed"] = new_speed
            else:
                # TODO
                raise NotImplementedError
        return new_state.copy()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("vehicle", type=str, help="name of vehicle to simulate")
    parser.add_argument(
        "--sqs",
        type=str,
        help="SQS endpoint URL if developing locally e.g. http://localhost:9324",
    )
    args = parser.parse_args()

    sqs_client = boto3.client("sqs", endpoint_url=args.sqs)
    sensor_funcs = {
        "speed": sensors.speed,
        #"air_quality_level": sensors.air_quality_level,
        "fuel_level": sensors.fuel_level,
        "lane_departure_system": sensors.lane_departure_system,
        #"light": sensors.light,
        "car_weight": sensors.car_weight,
        "power_level": sensors.power_level,
        "prec_level": sensors.prec_level,
        "proximity": sensors.proximity,
        #"signal_level": sensors.signal_level,
    }

    vs = VehicleSimulator(args.vehicle, sensor_funcs, sqs_client)
    while True:
        try:
            vs.tick()
        except KeyboardInterrupt:
            break


if __name__ == "__main__":
    main()
